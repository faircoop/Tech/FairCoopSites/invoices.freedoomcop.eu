# -*- coding: utf-8 -*-

{
    'name': 'Partner FreedomCoop Custom',
    'depends': ['base','account','report'],
    'author': 'Santi Punto0 - FairCooop',
    'category': 'Base',
    'description': """
Customization on the product model for FreedomCoop
    """,
    'website': 'https://punto0.org',
    'data': [
        'views/partner_view.xml',
        'views/report_invoice_custom.xml',
        'security/record_rules.xml',
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'auto_install': False,
}
