# -*- coding: utf-8 -*-

import logging
from openerp import models, fields, api, osv, _

_logger = logging.getLogger(__name__)


class ResPartner(models.Model):
    _inherit = 'res.partner'

    spark_id = fields.Char('Spark User Id', help="The Spark User Id", translate=False)
    faircoin_address = fields.Char('FairCoin user address', help="The FairCoin address for the user", translate=False)
    boc_id = fields.Char('BotC', help='The Bank of the Commons User Id', translate=False)
    betabank_id = fields.Char('Betabank User Id', help="The user id in betabank to print in the invoice", translate=False)
    prefered_payment = fields.Selection([('betabank','IBAN to BetaBank (Germany)'), ('boc','IBAN to Bank of the Commons (Spain)')],
                       help="Your prefered payment method",
                       default="betabank")
    is_member_boc = fields.Boolean('Membership of the BOTC', help="When checked the user is member of the Bank of the Commons")
