{
    'name' : 'Invoicing interface',
    'version': '1.0',
    'author' : 'Santi',
    'summary': 'Controller to invoices management in Odoo',
    'category': 'Account',
    'description':
        """
Invoicing Interface
=================
Crea controladores en Odoo para gestionar facturas desde otros sistemas
        """,
    'data': [
    ],
    'depends' : ['web','account'],
    'application': False,
}
