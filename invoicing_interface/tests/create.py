import json
import urllib
import urllib2
import logging
from requests import Request,Session

# ['name', 'reference', 'comment', 'date_due', 'partner_id', 'company_id', 'account_id', 'currency_id', 'payment_term', 'user_id', 'fiscal_position']

base_url = 'http://devmarket.punto0.org'
user = 'contable'
password = 'haciendofacturas'
db = "invoices"

def login():
    # login
    url = "%s/web/session/authenticate" % base_url
    s = Session()
    data = {
        'jsonrpc': '2.0',
        'params': {
            'context': {},
            'db': db,
            'login': user,
            'password': password,
        },
    }
    headers = {
        'Content-type': 'application/json'
    }
    req = Request('POST',url,data=json.dumps(data),headers=headers)
    prepped = req.prepare()
    resp = s.send(prepped)
    r_data = json.loads(resp.text)
    return r_data['result']['session_id']

if __name__ == '__main__':
    session_id = login()
    url = "%s/invoice_controller/create" % base_url
    headers = {'content-type':'application/html'}
    # 'account_id', 'currency_id', 'payment_term', 'fiscal_position'
    data_json = {
        'db': db, #nombre de la base de datos, necesario para multidatabases sites
        'name':' test',
        'reference':  'comment',
        'partner_id':8, # cliente
        'company_id':1, # cia datos factura
        'user_id':5,  # usuario que crea la factura, debe tener permisos y es el que hace login
        'type': 'out_invoice' # factura de venta
    }
    data_encoded =  urllib.urlencode(data_json)
    logging.warning("Data encoded to send : %s" %data_encoded)
    req = urllib2.Request(url, data_encoded, headers)
    try:
        response_stream = urllib2.urlopen(req)
        logging.warning('Got Response : %s' %(response_stream))
    except urllib2.HTTPError as e:
        logging.error("ERROR: cannot do callback in %s with data %s" %(url, data_json))
        logging.error("ERROR: code : %s" %e.code)
    except urllib2.URLError as e:
        logging.error('ERROR: Can not contact with %s' %url)
        logging.error('ERROR: Reason : %s ' % e.reason)
    except ValueError, e:
        logging.error(e)
        logging.error("ERROR: cannot do callback in %s with data %s" %(url, data_json))
