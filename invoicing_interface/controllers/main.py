# -*- coding: utf-8 -*-
import werkzeug
import openerp
import logging
import pprint
import urlparse
from openerp import SUPERUSER_ID
from openerp import http
from openerp.http import request
from openerp.tools.translate import _
from openerp.addons.website.models.website import slug
# import openerp.addons.website_sale.controllers.main
from openerp.addons.web.controllers.main import login_redirect

_logger = logging.getLogger(__name__)

class InvoiceController(http.Controller):

    @http.route('/invoice_controller/create_json', type='json', auth='none')
    def create_json(self, **post):
        if not post:
            return {'pdf': None}
        _logger.debug('Creating invoice with post data %s', pprint.pformat(post))  # debug
        return {'pdf': pdf_url }

    # ['name', 'reference', 'comment', 'date_due', 'partner_id', 'company_id', 'account_id', 'currency_id', 'payment_term', 'user_id', 'fiscal_position']
    @http.route('/invoice_controller/create_url', type='http', auth='user')
    def create_url(self, **post):
        if not post:
            return {'pdf': None}
        _logger.debug('Creating invoice with post data %s', pprint.pformat(post))  # debug
        cr, uid, context = request.cr, request.uid, request.context
        values = {}
        #data_raw = request.httprequest.get_data()
        #values = urlparse.parse_qs(data_raw)
        #_logger.debug('Data decoded : %s' %values)
        _logger.debug('user_id %s' %post.get('user_id'))
        # values['invoice_line'] = self._refund_cleanup_lines(invoice.invoice_line)

        if 'journal_id' in post:
            journal = request.registry['account.journal'].browse(cr, int(post.get('user_id')), post['journal_id'])
        elif post.get('type', False) == 'out_invoice':
            journal = request.registry['account.journal'].search(cr, int(post.get('user_id')), [('type', '=', 'sale')], limit=1)
        else:
            journal = request.registry['account.journal'].search(cr, int(post.get('user_id')), [('type', '=', 'purchase')], limit=1)

        values['journal_id'] = journal[0]
        values['type'] = post.get('type')
        # values['date_invoice'] = date or fields.Date.context_today(invoice)
        values['state'] = 'draft'
        values['number'] = False
        # values['origin'] = invoice.number

        #if period_id:
        #    values['period_id'] = period_id
        #if description:
        values['name'] = post.get('name')
        values['partner_id'] = post.get('partner_id')
        values['company_id'] = post.get('company_id')
        values['account_id'] = '19' # sales account 
        id = request.registry['account.invoice'].create(cr, int(post.get('user_id')), values)
        _logger.debug('user_id %s' %post.get('user_id'))
        return werkzeug.utils.redirect('/')
