# -*- coding: utf-8 -*-

{
    'name': 'Web FreedomCoop Custom',
    'depends': ['web'],
    'author': 'Santi Punto0 - FairCooop',
    'category': 'Web',
    'description': """
Customization on the webs for the FreedomCoop invoices web
    """,
    'website': 'https://freedomcoop.eu',
    'data': [
        'views/templates.xml',
        'views/qweb.xml',
    ],
    'installable': True,
    'auto_install': False,
}
